# README #

Very simple set of bash scripts for AWS CLI. The goal is to enable easy management of multiple resources, be they EC2 instances, S3 buckets or other services, and particularly to handle resources across all regions. 

No effort has been made to refactor into common scripts at this stage. The approach has been to clone and hack. If someone wishes to undertake this work, please do it and submit a pull request. 

## A Quick Summary ##

Guidance is provided in the names. The EC2 tools were written first and are the model. ec2list lists all the instances for the current region, stripping away the enormous metadata returned and yielding only the InstanceId. ec2kill terminates all the instances for the current region. To terminate a single instance, use ec2list to list the InstanceIds and use the standard CLI commands to kill the one you want. 

Each of these tools then has a corresponding all regions version. ec2listall lists instances across all the active regions; ec2killall similarly terminates instances across all the regions. 

There are a couple of variations from these pairings. The tool awsall is a simple script to allow the user to execute a specific aws CLI command across all the regions. s3kill is set to use the default us-standard region, which allows use of the default s3.amazonaws.com endpoint. The queue management tools are region insensitive. 

If in doubt about a script, please look at the code. 

## A Quick Listing ##

### aws ###
* awsall <aws CLI command> - execute a specific aws CLI command across all regions 

### CloudWatch - Current Region ###
* cwlistalarms - list cloudwatch alarms for the current region 
* cwkillalarms - kill cloudwatch alarms for the current region 

### CloudWatch - All Regions ###
* cwlistallalarms - list cloudwatch alarms for all regions 
* cwkillallalarms - kill cloudwatch alarms for all regions 

### EC2 - Current Region ###
* ec2list - list ec2 instances for the current region 
* ec2kill - kill ec2 instances for the current region 

### EC2 - All Regions ###
* ec2listall - list ec2 instances for all regions 
* ec2killall - kill ec2 instances for all regions 

### EC2 - Current Region ###
* ec2list - list ec2 instances for the current region 
* ec2kill - kill ec2 instances for the current region 

### EC2 - All Regions ###
* ec2listall - list ec2 instances for all regions 
* ec2killall - kill ec2 instances for all regions 

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact